#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
simple conjugation of regular dutch verbs
http://www.dutchgrammar.com/en/?n=Verbs.01
"""

from optparse import OptionParser

class Verbum:
	"""generic verb methods"""
	def __init__(self, werkwoord):
		self.werkwoord = werkwoord

	def extract_stam(self):
		"""extracts the stem of the verb. only takes 3 rules into account."""
		# een stam eindigt nooit op twee dezelfe medeklinkers
		if self.werkwoord[-3] == self.werkwoord[-4]:
			self.stam = self.werkwoord[:-3]

		# de stam van een -iën werkwoord eindigt op `ie'
		elif self.werkwoord[-4:] == "iën":
			self.stam = self.werkwoord[:-3] + "e"	
		
		# een stam eindigt nooit op v of z
		elif self.werkwoord[-3] == "v":
			self.stam = self.werkwoord[:-3] + "f"
		elif self.werkwoord[-3] == "z":
			self.stam = self.werkwoord[:-3] + "s"

		else:
			self.stam = self.werkwoord[:-2]

	def to_string(self):
		"""writes out the conjugation to stdout"""
		# this would take a list of words as an argument.
		# it would then write out `pronomen - word' list.
		# the Verbum class would have the pronomen as attributes.
		pass

class Presens(Verbum):
	"""takes a verb and conjugates it in the present tense"""
	def schrijven(self):
		"""writes out the conjugated verb to stdout"""
		pronomen_singularis = ["ik", "jij/je", "u", "hij/zij/ze"]
		pronomen_pluralis = ["wij/we", "jullie", "zij/ze"]
		
		for pronom in pronomen_singularis:
			if pronom == "ik":
				print "%s\t%s()" % (pronom, self.stam)
			else:
				print "%s\t%s(%s)" % (pronom, self.stam, "t")

		for pronom in pronomen_pluralis:
			# bug re rule #1, try `pakken'
			print "%s\t%s(%s)" % (pronom, self.stam, "en")

if __name__ == "__main__":
	gui = OptionParser()
	gui.add_option("-w", "--werkwoord", action="store", dest="werkwoord")
	(options, args) = gui.parse_args()
	verbum = Presens(options.werkwoord)

	# simple sanity checks, the word `t' is not a verb :)
	try:
		verbum.werkwoord[-4]
	except IndexError:
		gui.error("option -w, the verb needs more letters")
	if verbum.werkwoord[-4:] != "iën":
		if verbum.werkwoord[-2:] != "en":
			gui.error("option -w: the ending of the verb must be \"-en\"")	

	verbum.extract_stam()
	verbum.schrijven()

# vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4
